import { dbQuery, syncDB } from "./utils/db"
import { random } from "faker";
import { get } from "request-promise-native";
import { expect } from "chai";

const TRACKER_HOST = 'localhost'
const TRACKER_PORT = 3000

let north = random.number({ min: 1, max:20 })
let south = random.number({ min: 1, max:20 })
let west = random.number({ min: 1, max:20 })
let east = random.number({ min: 1, max:20 })

describe('Tracker Server', function() {
    this.timeout(30000)

    before(function(done) {
        setTimeout(done, 15000);
    });

    this.beforeEach(async function() {
        await syncDB()

        beforeEach(async function (){

            await dbQuery({
                type: "remove",
                table: "track_events"
            });
            
            this.tracks = (await dbQuery({
                type: "insert", 
                table: "track_events",
                values: {
                    "rider_id": 4,
                    "north": north,
                    "south": south,
                    "west": west,
                    "east": east,
                    "createdAt": new Date(),
                    "updatedAt": new Date()
                },
                returning: [
                    "rider_id",
                    "north",
                    "south",
                    "east",
                    "west",
                    "createdAt"
                ]                
            }))[0][0];
            
        })

        describe('Movement', function() {
            it('harusnya memberikan data suatu rider', async function(){
                console.log('test')
                const res = await get(
                    `http://${TRACKER_HOST}: ${TRACKER_PORT}/movement/4`,
                    { json:true }
                )
                expect(res.ok).to.be.true
                expect(res.logs).to.deep.eq({
                    rider_id:4,
                    north: north,
                    east: east,
                    west: west,
                    south: south

                })
                // .to.have.all.keys('rider_id', 'north', 'south','east','west','createdAt');
                // 

            })
        })
    })
})